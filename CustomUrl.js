import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  BackHandler,
  Button,
  Linking,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  ToastAndroid,
  AsyncStorage
} from "react-native";
import FLoatingLabel from "react-native-floating-labels";

export default class CustomUrl extends Component {
  constructor() {
    super();
    this.state = {
      title: "",
      longurl: "",
      custurl: "",
      sho: "",
      isLoading: false,
      dataSource: []
    };
  }

  // Back Button
  // componentDidMount() {
  //   BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
  // }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
  }

  handleBackButton() {
    ToastAndroid.show("Back button is pressed", ToastAndroid.SHORT);
    return true;
  }

  // Text input
  updateTitle(text, field) {
    if ((field = "title")) {
      this.setState({
        title: text
      });
    }
  }
  updateValue(text, field) {
    if (field == "longurl") {
      this.setState({
        longurl: text
      });
    }
    // console.warn(text)
  }

  updateCust(text, field) {
    if ((field = "custurl")) {
      this.setState({
        custurl: text
      });
    }
  }

  // Submit button for custom url
  submitUrl() {
    let data = {
      url: this.state.longurl !== "" ? this.state.longurl : "GAGAL",
      title: this.state.title,
      custom: this.state.custurl
    };
    let self = this;

    // console.warn(data);
    let url = `http:192.168.100.12:3000/customUrls/shortenCustom`;
    AsyncStorage.getItem("token", (error, tokenLog) => {
      fetch(url, {
        method: "POST",
        body: JSON.stringify(data),
        headers: new Headers({
          "Content-Type": "application/json",
          Authorization: "Bearer " + tokenLog
        })
      }).then(res =>
        res.json().then(val => {
          self.setState({
            sho: val
          });
          // console.warn("sho=", val);
        })
      );
    }).catch(error => console.warn("Error:", error));
  }

  logout() {
    this.props.navigation.navigate("Login");
  }

  // Sort item with flatlist
  renderItem = ({ item }) => {
    return (
      <View>
        <Text>{item.titleUrl}</Text>
        <Text>{item.url}</Text>
        <Text>{item.short_url}</Text>
      </View>
    );
  };

  async componentDidMount() {
    const url = `http:192.168.100.12:3000/customurls/listUrls`;
    // console.warn("COMPONENT DID MOUNT:", url);

    try {
      AsyncStorage.getItem("token", (error, tokenLog) => {
        // console.warn("TEST");

        fetch(url, {
          method : "POST",
          headers: new Headers({
            "Content-Type": "application/json",
            Authorization: "Bearer " + tokenLog
          })
        })
          .then(response => {
            // console.warn("RES IS RUNNING");
            // console.log("response:", response);
            response.json().then(responseJson => {
              // console.warn("RUNNING", responseJson);
              this.setState({
                dataSource: responseJson,
                isLoading: false
              });
            });
          })
          .catch(error => {
            console.log("COMPONENT MOUNT ERR : ", error);
          });
      });
    } catch (error) {
      console.log(error);
    }
  }

  renderSeparator = () => {
    return (
      <View style={{ height: 1, width: "100%", backgroundColor: "black" }} />
    );
  };

  render() {
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="blue" animating />
      </View>
    ) : (
      <View
        style={{
          paddingHorizontal: 30,
          flex: 1,
          backgroundColor: "#3498db"
        }}
      >
        <FLoatingLabel
          inputStyle={styles.inputAja}
          style={styles.input}
          returnKeyType="next"
          onChangeText={text => this.updateTitle(text, "title")}
        >
          Title
        </FLoatingLabel>
        <FLoatingLabel
          inputStyle={styles.inputAja}
          style={styles.input}
          returnKeyType="next"
          onChangeText={text => this.updateValue(text, "longurl")}
        >
          Url
        </FLoatingLabel>
        <FLoatingLabel
          inputStyle={styles.inputAja}
          style={styles.input}
          returnKeyType="done"
          onChangeText={text => this.updateCust(text, "custurl")}
        >
          Custom your URL
        </FLoatingLabel>
        <View style={{ justifyContent: "center" }}>
          <Button title="SUBMIT" onPress={() => this.submitUrl()} />
          <Button title="LOGOUT" onPress={() => this.logout()} />
        </View>
        <Text
          style={styles.textAja}
          onPress={() =>
            Linking.openURL(
              `http://192.168.100.12:3000/r/${this.state.sho.short_url}`
            )
          }
        >
          {this.state.sho !== ""
            ? `http://192.168.100.12:3000/r/${this.state.sho.short_url}`
            : "Your custom url show here"}
        </Text>
        <FlatList
          data={this.state.dataSource}
          renderItem={this.renderItem}
          extraData={this.state}
          keyExtractor={(item, index) => index}
          ItemSeparatorComponent={this.renderSeparator}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    margin: 10,
    borderBottomWidth: 1,
    borderBottomColor: "white",
    marginBottom: 20
  },
  textAja: {
    fontSize: 20,
    textAlign: "center",
    marginTop: 15,
    color: "white"
  },
  inputAja: {
    borderWidth: 0
  }
});
