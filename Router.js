import { createStackNavigator } from "react-navigation";
import Login from "./Login";
import Register from "./Register";
import ShortUrl from "./ShortUrl";
import CustomUrl from "./CustomUrl";

export const Router = createStackNavigator({
  Login: {
    screen: Login,
    navigationOptions: {
      header: null
    }
  },
  Register: {
    screen: Register,
    navigationOptions: {
      header: null
    }
  },
  ShortUrl: {
    screen: ShortUrl
  },
  CustomUrl: {
    screen: CustomUrl,
    navigationOptions: {
      header: null
    }
  }
});
