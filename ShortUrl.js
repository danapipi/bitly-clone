import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  Linking
} from "react-native";
import FLoatingLabel from "react-native-floating-labels";

export default class ShortUrl extends Component {
  constructor() {
    super();
    this.state = {
      longurl: "",
      sho: ""
    };
  }

  updateValue(text, field) {
    if (field == "longurl") {
      this.setState({
        longurl: text
      });
    }
    // console.warn(text)
  }

  submitUrl() {
    let data = {};
    data.url = this.state.url;
    let self = this;

    console.warn(data);
    let url = `http:192.168.100.12:3000/customurls/short/${this.state.longurl}`;
    fetch(url).then(res => {
      res.json().then(val => {
        self.setState({
          sho: val
        });
        console.warn(val);
      });
    });
  }

  render() {
    return (
      <View
        style={{
          paddingHorizontal: 40,
          alignItems: "center",
          backgroundColor: "#3498db",
          flex:1,
          justifyContent:"center"
        }}
      >
        <FLoatingLabel
          inputStyle={styles.inputAja}
          style={styles.input}
          returnKeyType="done"
          onChangeText={text => this.updateValue(text, "longurl")}
        >
          URL
        </FLoatingLabel>
        <Button title="SUBMIT" onPress={() => this.submitUrl()} />
        <Text
          style={{ marginTop: 40 }}
          onPress={() =>
            Linking.openURL(
              `http://192.168.100.12:3000/customurls/short/${this.state.sho.short_url}`
            )
          }
        >
          {this.state.sho !== ""
            ? `http://192.168.100.12:3000/customurls/short/${this.state.sho.short_url}`
            : "Your short url"}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    marginVertical: 20,
    width: 300,
    borderBottomWidth: 1,
    borderBottomColor: "white"
  },
  inputAja: {
    borderWidth: 0
  }
});
