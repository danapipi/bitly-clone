import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  Image,
  ScrollView,
  AsyncStorage
} from "react-native";

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      username: "",
      password: ""
    };
  }

  updateValue(text, field) {
    if (field == "username") {
      this.setState({ username: text });
    } else if (field == "password") {
      this.setState({ password: text });
    }
    // console.warn(text)
  }

  submit() {
    let data = {};
    (data.username = this.state.username),
      (data.password = this.state.password);
      // console.warn(data);
    let url = "http:192.168.100.12:3000/users/login";

    fetch(url, {
      method: "POST",
      body: JSON.stringify(data),
      headers: new Headers({
        "Content-Type": "application/json"
      })
    })
      .then(res => {
        if (res.ok) {
          res.json().then(data => AsyncStorage.setItem("token",data.token));
          this.props.navigation.navigate("CustomUrl");
        }
      })
      .catch(error => console.warn("Error:", error));
    // .then(response => console.log("Success:", response));
  }

  submitReg() {
    this.props.navigation.navigate("Register");
  }

  submitUrl() {
    this.props.navigation.navigate("ShortUrl");
  }

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            justifyContent: "space-between",
            flex: 1
          }}
        >
          <View />
          <View>
            <Image
              source={require("./Logo.png")}
              style={{ width: 120, height: 120, alignSelf: "center" }}
              resizeMode="stretch"
            />
            <Text style={styles.title}>An app for shorten your URL</Text>
          </View>
          <View style={{ paddingHorizontal: 40 }}>
            <TextInput
              placeholder="Email"
              keyboardType="email-address"
              autoCapitalize="none"
              autoCorrect={false}
              onSubmitEditing={() => this.passwordInput.focus()}
              returnKeyType="next"
              onChangeText={text => this.updateValue(text, "username")}
              style={styles.input}
            />
            <TextInput
              placeholder="Password"
              returnKeyType="done"
              ref={input => (this.passwordInput = input)}
              secureTextEntry={true}
              onChangeText={text => this.updateValue(text, "password")}
              style={styles.input}
            />
            <View style={{marginBottom:10}}>
              <Button title="LOGIN" onPress={() => this.submit()} />
              <View style={{ height: 10 }} />
              <Button title="REGISTER" onPress={() => this.submitReg()} />
              <View style={{ height: 10 }} />
              <Button
                title="I DON'T WANT TO LOGIN"
                onPress={() => this.submitUrl()}
              />
            </View>
            <View style={{ height: 12 }} />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#3498db",
    flex: 1
  },
  logoContainer: {
    alignItems: "center",
    justifyContent: "center"
  },
  title: {
    color: "#FFF",
    marginTop: 10,
    textAlign: "center",
    fontSize: 18
  },
  input: {
    fontSize: 20,
    color: "white",
    marginBottom: 13,
    textAlign: "center",
    justifyContent: "center",
    backgroundColor: "#74b9ff"
  }
});
