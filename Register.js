import React, { Component } from "react";
import { StyleSheet, View, ScrollView, Button } from "react-native";
import FLoatingLabel from "react-native-floating-labels";

export default class Register extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      email: "",
      username: "",
      password: "",
      vpassword: ""
    };
  }

  updateValue(text, field) {
    if (field == "name") {
      this.setState({ name: text });
    } else if (field == "email") {
      this.setState({ email: text });
    } else if (field == "username") {
      this.setState({ username: text });
    } else if (field == "password") {
      this.setState({ password: text });
    } else if (field == "vpassword") {
      this.setState({ vpassword: text });
    }
    // console.warn(text)
  }

  submit() {
    let data = {};
    (data.name = this.state.name),
      (data.email = this.state.email),
      (data.username = this.state.username),
      (data.password = this.state.password),
      (data.password2 = this.state.vpassword);
    // console.warn(data);
    let url = "http:192.168.100.12:3000/users/register";

    fetch(url, {
      method: "POST",
      body: JSON.stringify(data),
      headers: new Headers({
        "Content-Type": "application/json"
      })
    })
      .then(res => {
        res.text().then(data => console.warn(data));
        if (res.status === 200) this.props.navigation.navigate("Login");
      })
      .catch(error => console.log("Error:", error));
    // .then(response => console.log("Success:", response))
  }

  render() {
    return (
      <ScrollView
        contentContainerStyle={{
          flex: 1,
          backgroundColor: "#3498db"
        }}
      >
        <View
          style={{
            flex: 1,
            paddingHorizontal: 30,
            alignItems: "center",
            justifyContent: "center",
            height: 600,
            backgroundColor: "#3498db"
          }}
        >
          <FLoatingLabel
            inputStyle={styles.inputAja}
            autoCorrect={false}
            style={styles.input}
            returnKeyType="next"
            onChangeText={text => this.updateValue(text, "name")}
          >
            Name
          </FLoatingLabel>
          <FLoatingLabel
            inputStyle={styles.inputAja}
            keyboardType="email-address"
            autoCorrect={false}
            style={styles.input}
            returnKeyType="next"
            onChangeText={text => this.updateValue(text, "email")}
          >
            Email
          </FLoatingLabel>
          <FLoatingLabel
            inputStyle={styles.inputAja}
            style={styles.input}
            returnKeyType="next"
            onChangeText={text => this.updateValue(text, "username")}
          >
            Username
          </FLoatingLabel>
          <FLoatingLabel
            inputStyle={styles.inputAja}
            autoCapitalize="none"
            returnKeyType="next"
            style={styles.input}
            secureTextEntry={true}
            onChangeText={text => this.updateValue(text, "password")}
          >
            Password
          </FLoatingLabel>
          <FLoatingLabel
            inputStyle={styles.inputAja}
            style={styles.input}
            returnKeyType="done"
            secureTextEntry={true}
            onChangeText={text => this.updateValue(text, "vpassword")}
          >
            Verify Password
          </FLoatingLabel>
          <Button title="SUBMIT" onPress={() => this.submit()} />
        </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  input: {
    width: 300,
    borderBottomWidth: 1,
    borderBottomColor: "white",
    marginBottom: 20
  },
  inputAja: {
    borderWidth: 0
  }
});
