import React, { Component } from "react";
import { StyleSheet, Text, View, TextInput, Button, Image } from "react-native";
import { Router } from "./Router";

export default class App extends Component {
  render() {
    return <Router />;
  }
}
